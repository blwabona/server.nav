export RUN_PATH="/home/WimtAdministrator/server.nav/"
currentDateTime=$(date +"%F")"_T_"$(date +"%T")
java -jar ${RUN_PATH}graphhopper-web-0.5.0-with-dep.jar jetty.resources=webapp config=${RUN_PATH}config-example.properties osmreader.osm=${RUN_PATH}south-africa-and-lesotho-latest.osm.pbf >logs_$currentDateTime.txt 2>&1 &
